//
//  TableViewController.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 15/12/2023.
//

import UIKit

class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var listDataComment: [DataComment] = [
        DataComment(paren: "comment 1, alocomment 1, alocomment 1, alo, comment 1, alocomment 1, alocomment 1, alo"),
        DataComment(paren: "comment 2, alocomment 2, alocomment 2, alo", childer: ["chidren 1 alo alo", "chidren 2 alo alo", "comment 2, alocomment 2, alocomment 2, alo"]),
        DataComment(paren: "comment 3, alocomment 3, alocomment 3, alo", childer: ["comment 2, alocomment 2, alocomment 2, alo"]),
        
        DataComment(paren: "comment 1, alocomment 1, alocomment 1, alo, comment 1, alocomment 1, alocomment 1, alo"),
        DataComment(paren: "comment 2, alocomment 2, alocomment 2, alo", childer: ["chidren 1 alo alo", "chidren 2 alo alo", "comment 2, alocomment 2, alocomment 2, alo"]),
        DataComment(paren: "comment 3, alocomment 3, alocomment 3, alo", childer: ["comment 2, alocomment 2, alocomment 2, alo"]),
        DataComment(paren: "comment 1, alocomment 1, alocomment 1, alo, comment 1, alocomment 1, alocomment 1, alo"),
        DataComment(paren: "comment 2, alocomment 2, alocomment 2, alo", childer: ["chidren 1 alo alo", "chidren 2 alo alo", "comment 2, alocomment 2, alocomment 2, alo"]),
        DataComment(paren: "comment 3, alocomment 3, alocomment 3, alo", childer: ["comment 2, alocomment 2, alocomment 2, alo"]),
    ]
    var listComment:[Comment] = []
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: "tableTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "tableTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        
        processData()
        // Do any additional setup after loading the view.
    }
    
    func processData(){
        for item in listDataComment{
            if item.paren != nil {
                listComment.append(Comment(paren: item.paren))
            }
            if item.childer?.count != 0{
                if let listChid = item.childer {
                    for chid in listChid {
                        listComment.append(Comment(childer: chid))
                    }
                }
            }
        }
        tableView.reloadData()
        print(listComment.count)
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listComment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableTableViewCell", for: indexPath as IndexPath) as! tableTableViewCell
        let count = indexPath.row + 1
//        print(listComment[count].paren?.count)
        if(count < listComment.count){
            if(listComment[count].childer?.count != nil){
                let shape = UIBezierPath()
                let shapeLayer = CAShapeLayer()
                shape.lineWidth = 1
                shape.move(to: CGPoint(x: 12, y: 12))
                shape.addLine(to: CGPoint(x: 12, y: 50))
                shape.addLine(to: CGPoint(x: 52, y: 50))

                shapeLayer.path = shape.cgPath
                cell.layer.addSublayer(shapeLayer)
            }
        }
        let textLabel = listComment[indexPath.row].paren != nil ? listComment[indexPath.row].paren : listComment[indexPath.row].childer
        let const = listComment[indexPath.row].paren != nil ? 10 : 50
        cell.label.text = textLabel
        cell.left.constant = CGFloat(const)
        return cell
    }
    
    
}
