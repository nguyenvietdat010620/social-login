//
//  DrawShape.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 18/12/2023.
//

import Foundation
import UIKit

class Shape: UIView {
    let shapeLayer = CAShapeLayer()
    let maskLayer = CAShapeLayer()
    var rectanglePath = UIBezierPath()

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        backgroundColor = UIColor.clear

        // initial shape of the view
        rectanglePath = UIBezierPath(rect: bounds)

        // Create initial shape of the view
        shapeLayer.path = rectanglePath.cgPath
        shapeLayer.strokeColor = UIColor.black.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        layer.addSublayer(shapeLayer)

        //mask layer
        maskLayer.path = shapeLayer.path
        maskLayer.position =  shapeLayer.position
        layer.mask = maskLayer
    }

    func prepareForEditing(editing:Bool){

        let animation = CABasicAnimation(keyPath: "path")
        animation.duration = 2

        // Your new shape here
        animation.toValue = UIBezierPath(ovalIn: bounds).cgPath
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)

        // The next two line preserves the final shape of animation,
        // if you remove it the shape will return to the original shape after the animation finished
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false

        shapeLayer.add(animation, forKey: nil)
        maskLayer.add(animation, forKey: nil)
      }


}
