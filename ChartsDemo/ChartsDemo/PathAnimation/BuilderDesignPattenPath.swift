//
//  BuilderDesignPattenPath.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 19/12/2023.
//

import Foundation
import QuartzCore
import UIKit

class BuilderDesignPattenPath {
    let emitterCellBuider = CAEmitterCell()
    
    public func setImage(imageName: String) -> BuilderDesignPattenPath{
        emitterCellBuider.contents = UIImage(named: imageName)?.cgImage
        return self
    }
    
    public func rangeSize(sizeMin: Double, sizeMax: Double) -> BuilderDesignPattenPath{
        emitterCellBuider.scale = sizeMin
        emitterCellBuider.scaleRange = sizeMax
        return self
    }
    
    public func timeLifeOfCell(timeLife: Float) -> BuilderDesignPattenPath {
        emitterCellBuider.lifetime = timeLife
        return self
    }
    
    public func numberCellInScreen(numberCell: Float) -> BuilderDesignPattenPath {
        emitterCellBuider.birthRate = numberCell
        return self
    }
    
    public func rangeVelocity(velocityMin: CGFloat, velocityMax: CGFloat) -> BuilderDesignPattenPath {
        emitterCellBuider.velocity = velocityMin
        emitterCellBuider.velocityRange = velocityMax
        return self
    }
    
    public func yAcceleration(yAcceleration: CGFloat) -> BuilderDesignPattenPath {
        emitterCellBuider.yAcceleration = yAcceleration
        return self
    }
    //huong roi
    public func emissionRange(emissionRange: CGFloat) -> BuilderDesignPattenPath {
        emitterCellBuider.emissionRange = emissionRange
        return self
    }
    
    public func build () -> CAEmitterCell {
        return self.emitterCellBuider
    }
}

