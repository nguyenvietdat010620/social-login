//
//  PathAnimationViewController.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 18/12/2023.
//

import UIKit



class PathAnimationViewController: UIViewController {

    @IBOutlet weak var labelNumer: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNumer()
        setuoUISnowAndRain()
//        showOverlay()
        // Do any additional setup after loading the view.
    }
    
    
    func setupNumer() {
        labelNumer.text =  NSString(format:"23%@", "\u{00B0}") as String
    }
    
    @IBAction func action(_ sender: Any) {
        ModalViewController.present(initialView: self,delegate: self)
    }
    
    func setuoUISnowAndRain() {
        let emitterLayer = CAEmitterLayer()
        emitterLayer.emitterPosition = CGPoint(x: self.view.bounds.size.width / 2, y: self.view.bounds.origin.y)
        emitterLayer.emitterSize = CGSize(width: self.view.bounds.size.width, height: 0)
        emitterLayer.emitterShape = CAEmitterLayerEmitterShape.line
//        self.view.layer.addSublayer(emitterLayer)
        
        
        self.view.layer.addSublayer(emitterLayer)
        let emitterSnowBuilderPatten = BuilderDesignPattenPath()
            .setImage(imageName: "snow")
            .rangeSize(sizeMin: 0.02, sizeMax: 0.05)
            .timeLifeOfCell(timeLife: 5.0)
            .numberCellInScreen(numberCell: 50)
            .rangeVelocity(velocityMin: 50, velocityMax: 100)
            .yAcceleration(yAcceleration: 250)
            .emissionRange(emissionRange: CGFloat(M_PI))
            .build()
        
        let emitterRainBuilderPatten = BuilderDesignPattenPath()
            .setImage(imageName: "rain")
            .rangeSize(sizeMin: 0.02, sizeMax: 0.05)
            .timeLifeOfCell(timeLife: 5.0)
            .numberCellInScreen(numberCell: 50)
            .rangeVelocity(velocityMin: 50, velocityMax: 100)
            .yAcceleration(yAcceleration: 250)
            .emissionRange(emissionRange: CGFloat(M_PI))
            .build()
        emitterLayer.emitterCells = [emitterSnowBuilderPatten, emitterRainBuilderPatten]
    }
    
    
    
}
extension PathAnimationViewController: PopUpModalDelegate {
    func didTapCancel() {
//        self.dismiss(animated: true)
    }
    
    func didTapAccept(){
        
    }
}
