//
//  TestViewController.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 14/12/2023.
//

import UIKit

class TestViewController: UIViewController {
    let shapeLayer = CAShapeLayer()
    var string: String?
    var arrowView: ArrowView?
    
    var isPause: Bool = true
    private var reverseAnimation = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        arrowView = ArrowView()
        drawShape()
        //        run()
        // Do any additional setup after loading the view.
    }
    
    
    
    func drawShape() {
        let shape = UIBezierPath()
        shape.move(to: CGPoint(x: 10, y: 0))
        shape.addLine(to: CGPoint(x: 10, y: 20))
        shape.addLine(to: CGPoint(x: 25, y: 10))
        shape.close()
        shapeLayer.path = shape.cgPath
        
        let viewShape = UIView()
        viewShape.frame = CGRect(x: 150, y: 400, width: shape.bounds.width, height: shape.bounds.height)
        viewShape.layer.addSublayer(shapeLayer)
        viewShape.layer.transform = CATransform3DMakeScale(2, 2, 1)
        
        
        view.addSubview(viewShape)
        
        //        SVG parsing for z data isn't supported yet
    }
    
    func thunder() -> UIBezierPath {
        let shape = UIBezierPath()
        shape.move(to: CGPoint(x: 7, y: 2))
        shape.addLine(to: CGPoint(x: 7, y: 13))
        shape.addLine(to: CGPoint(x: 10, y: 13))
        shape.addLine(to: CGPoint(x: 10, y: 22))
        shape.addLine(to: CGPoint(x: 17, y: 10))
        shape.addLine(to: CGPoint(x: 13, y: 10))
        shape.addLine(to: CGPoint(x: 17, y: 2))
        shape.close()
        return shape
    }
    
    func star() -> UIBezierPath {
        let shape = UIBezierPath()
        shape.move(to: CGPoint(x: 12, y: 17.27))
        shape.addLine(to: CGPoint(x: 18.18, y: 21))
        shape.addLine(to: CGPoint(x: 16.54, y: 13.97))
        shape.addLine(to: CGPoint(x: 22, y: 9.24))
        shape.addLine(to: CGPoint(x: 14.81, y: 8.63))
        shape.addLine(to: CGPoint(x: 12, y: 2))
        shape.addLine(to: CGPoint(x: 9.19, y: 8.63))
        shape.addLine(to: CGPoint(x: 2, y: 9.24))
        shape.addLine(to: CGPoint(x: 7.46, y: 13.97))
        shape.addLine(to: CGPoint(x: 5.82, y: 21))
        shape.close()
        return shape
    }
    
    func play()-> UIBezierPath {
        let shape = UIBezierPath()
        shape.move(to: CGPoint(x: 10, y: 0))
        shape.addLine(to: CGPoint(x: 10, y: 20))
        shape.addLine(to: CGPoint(x: 25, y: 10))
        shape.close()
        return shape
    }
    
    func pause() -> UIBezierPath {
        let shape = UIBezierPath()
        shape.move(to: CGPoint(x: 0, y: 0))
        shape.addLine(to: CGPoint(x: 0, y: 18))
        shape.addLine(to: CGPoint(x: 10, y: 18))
        shape.addLine(to: CGPoint(x: 10, y: 0))
        shape.addLine(to: CGPoint(x: 20, y: 0))
        shape.addLine(to: CGPoint(x: 20, y: 18))
        shape.addLine(to: CGPoint(x: 30, y: 18))
        shape.addLine(to: CGPoint(x: 30, y: 0))
        shape.close()
        return shape
    }
    
    @IBAction func actionButton(_ sender: Any) {
        let animation = CABasicAnimation(keyPath: "path")
        if(isPause){
            var newShapePath = pause().cgPath
            animation.toValue = newShapePath
        }
        else{
            var newShapePath = play().cgPath
            animation.toValue = newShapePath
        }
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.delegate = self
        self.shapeLayer.add(animation, forKey: "path")
        
    }
    
    func changeShape() {
        if(isPause){
            self.shapeLayer.path = pause().cgPath
            isPause = !isPause
        }
        else{
            self.shapeLayer.path = play().cgPath
            isPause = !isPause
        }
        
    }
    
}

extension TestViewController: CAAnimationDelegate{
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            self.changeShape()
        }
    }
}
