//
//  AppDelegate.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 13/12/2023.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FirebaseCore
import FirebaseFirestore
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

//clientId gglogin:424740691085-u3kfbkgrbi2oirhipnshoauj8is2ikh7.apps.googleusercontent.com

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        GIDSignIn.sharedInstance.configuration?.clientID = "424740691085-u3kfbkgrbi2oirhipnshoauj8is2ikh7.apps.googleusercontent.com"
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        FirebaseApp.configure()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func application(_ app: UIApplication,open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//      var handled: Bool
//
//      handled = GIDSignIn.sharedInstance.handle(url)
//      if handled {
//        return true
//      }
//        var faceHanled: Bool
//        faceHanled = ApplicationDelegate.shared.application(app, open: url, sourceApplication: (options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String),annotation: [UIApplication.OpenURLOptionsKey.annotation])
//        if faceHanled {
//            return true
//        }
        
      // Handle other custom URL types.

      // If not handled by this app, return false.
      return false
    }

}

