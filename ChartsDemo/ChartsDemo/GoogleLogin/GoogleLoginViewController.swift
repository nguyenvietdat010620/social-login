//
//  GoogleLoginViewController.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 16/12/2023.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit

class GoogleLoginViewController: UIViewController  {
    
    @IBOutlet weak var imageAva: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func LoginFacebook(_ sender: Any) {
        var login = LoginManager()
        guard let configuration = LoginConfiguration(
            permissions:["email", "user_friends", "user_birthday", "user_age_range", "user_gender", "user_location", "user_hometown", "user_link"],
            tracking: .limited,
            nonce: "123"
        )
        else {
            return
        }
        login.logIn(configuration: configuration) { res in
            switch res{
            case .cancelled:
                print()
            case .success:
                print(Profile.current?.userID)
                
            case .failed(_):
                print()
            }
        }
    }
    @IBAction func LoginAction(_ sender: Any) {
        print("asdasd")
        GIDSignIn.sharedInstance.signIn(
            withPresenting: self) { signInResult, error in
                guard let result = signInResult else {
                    // Inspect error
                    print("err")
                    return
                }
                print(" If sign in succeeded, display the app's main content View", 
                      signInResult?.user.userID, "token",
                      signInResult?.user.accessToken,
                      signInResult?.user.profile?.imageURL(withDimension: 120)?.absoluteString )
                if let url = signInResult?.user.profile?.imageURL(withDimension: 120)?.absoluteString {
                    ImageLoader.loadImage(from: url) { image in
                        DispatchQueue.main.async {
                            self.imageAva.image = image
                        }
                    }
                }
                
                // If sign in succeeded, display the app's main content View.
            }
        
    }
    
}

class ImageLoader {
    static func loadImage(from urlString: String, completion: @escaping (UIImage?) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completion(nil)
                return
            }
            
            let image = UIImage(data: data)
            completion(image)
        }.resume()
    }
}
