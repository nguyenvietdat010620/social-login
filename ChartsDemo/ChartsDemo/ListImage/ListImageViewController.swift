//
//  ListImageViewController.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 21/12/2023.
//

import UIKit

class ListImageViewController: UIViewController {
    let darkShadow = CALayer()
    let lightShadow = CALayer()
    var listImage = [UIImage(named:"1"),UIImage(named:"2"),UIImage(named:"3"),UIImage(named:"4"),UIImage(named:"5"),UIImage(named:"6"),UIImage(named:"7"),UIImage(named:"8"),UIImage(named:"9"),UIImage(named:"15")]
    var sizeScreen = UIScreen.main.bounds
    var sizeView = 300
    var oldheigh = 300
    var oldwidth = UIScreen.main.bounds.width
    var frameX = 0
    var frameY = 0
    var space = 5
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor  = UIColor(red: 225/255, green: 225/255, blue: 235/255, alpha: 1)
        // Do any additional setup after loading the view.
        setupUI()
        setupButton()
    }
    
    func setupButton() {
        var button = UIView()
        button.backgroundColor = UIColor(red: 225/255, green: 225/255, blue: 235/255, alpha: 1)
        button.layer.cornerRadius = 15
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 25)
        button.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        darkShadow.frame = button.bounds
        darkShadow.cornerRadius = 15
        darkShadow.backgroundColor = UIColor(red: 225/255, green: 225/255, blue: 235/255, alpha: 1).cgColor
        darkShadow.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        darkShadow.shadowOffset = CGSize(width: 10, height: 10)
        darkShadow.shadowOpacity = 1
        darkShadow.shadowRadius = 15
        button.layer.insertSublayer(darkShadow, at: 0)
        lightShadow.frame = button.bounds
        lightShadow.cornerRadius = 15
        lightShadow.backgroundColor = UIColor(red: 225/255, green: 225/255, blue: 235/255, alpha: 1).cgColor
        lightShadow.shadowColor = UIColor.white.withAlphaComponent(0.9).cgColor
        lightShadow.shadowOffset = CGSize(width: -5, height: -5)
        lightShadow.shadowOpacity = 1
        lightShadow.shadowRadius = 15
        button.frame = CGRect(x: 50, y: 500, width: 100, height: 100)
        button.layer.insertSublayer(lightShadow, at: 0)
        self.view.addSubview(button)
    }
    
    @objc func didTapCancel(_ btn: UIButton) {
//        self.delegate?.didTapCancel()
        print("asdas", btn)
    }
    
    func setupUI() {
        var view = UIView()
        
        view.frame = CGRect(x: 0, y: 50, width: Int(sizeScreen.width), height: sizeView)
        for i in 0...4{
            var imagePic = UIImageView()
            let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapCancel))
            imagePic.isUserInteractionEnabled = true
            imagePic.addGestureRecognizer(gesture)
            if let image = listImage[i] {
                let resizedImage = resizeImage(image: image, targetSize: setUpDequy(numberImage: i))
                imagePic.image = resizedImage
                imagePic.frame = CGRect(x: frameX, y: frameY, width: Int(oldwidth), height: oldheigh)
                view.addSubview(imagePic)
            }
            
        }
        self.view.addSubview(view)
    }
    
    func setUpDequy(numberImage: Int) -> CGSize {
        if (numberImage == 0) {
            frameX = 0
            frameY = 0
            let high = oldheigh
            let width = oldwidth / 2
            oldwidth = oldwidth / 2
            return CGSize(width: width, height: CGFloat(high))
        }
        else if (numberImage > 0 && numberImage < 4){
            if((numberImage) % 2 == 0 ){
                let width = oldwidth / 2
                let high = oldheigh
                oldwidth = oldwidth / 2
//                frameX = frameX + Int(oldwidth) + space
                frameY = frameY + Int(oldheigh) + space
                print("number", numberImage, oldheigh)
                return CGSize(width: CGFloat(width), height: CGFloat(high))
            }
            else{
                let high = oldheigh / 2
                let width = oldwidth
                oldheigh = oldheigh / 2
//                frameY = frameY + Int(oldheigh) + space
                frameX = frameX + Int(oldwidth) + space
                print("number", numberImage, oldwidth)
                return CGSize(width: CGFloat(width), height: CGFloat(high))
            }
        }
        else if (numberImage == 4){
            frameY = frameY + Int(oldheigh) + space
            return CGSize(width: CGFloat(oldwidth + oldwidth), height: CGFloat(oldheigh))
        }
        return CGSize()
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
        
        var newSize: CGSize
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

}
