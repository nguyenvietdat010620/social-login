//
//  OverlaySnow.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 19/12/2023.
//

import Foundation
import UIKit

class OverlaySnow: UIViewController {
    var viewMain = UIView()
    
    class var sharedInstance: OverlaySnow {
            struct Static {
                static let instance: OverlaySnow = OverlaySnow()
            }
            return Static.instance
        }
    
    func setupOverlay(view: UIView) {
        let emitterLayer = CAEmitterLayer()
        emitterLayer.emitterPosition = CGPoint(x: view.bounds.size.width / 2, y: view.bounds.origin.y)
        emitterLayer.emitterSize = CGSize(width: self.view.bounds.size.width, height: 0)
        emitterLayer.emitterShape = CAEmitterLayerEmitterShape.line
        self.view.layer.addSublayer(emitterLayer)
        
        let emitterSnowBuilderPatten = BuilderDesignPattenPath()
            .setImage(imageName: "snow")
            .rangeSize(sizeMin: 0.02, sizeMax: 0.05)
            .timeLifeOfCell(timeLife: 5.0)
            .numberCellInScreen(numberCell: 50)
            .rangeVelocity(velocityMin: 50, velocityMax: 100)
            .yAcceleration(yAcceleration: 250)
            .emissionRange(emissionRange: CGFloat(M_PI))
            .build()
        
        let emitterRainBuilderPatten = BuilderDesignPattenPath()
            .setImage(imageName: "rain")
            .rangeSize(sizeMin: 0.02, sizeMax: 0.05)
            .timeLifeOfCell(timeLife: 5.0)
            .numberCellInScreen(numberCell: 50)
            .rangeVelocity(velocityMin: 50, velocityMax: 100)
            .yAcceleration(yAcceleration: 250)
            .emissionRange(emissionRange: CGFloat(M_PI))
            .build()
        emitterLayer.emitterCells = [emitterSnowBuilderPatten, emitterRainBuilderPatten]
    }
    
    func showOverlay() {
        
    }
    
}
