//
//  ModalViewController.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 18/12/2023.
//

import UIKit
public protocol PopUpModalDelegate: AnyObject {
    func didTapCancel()
    func didTapAccept()
}

class ModalViewController: UIViewController, UIGestureRecognizerDelegate {
    let maximumContainerHeight: CGFloat = UIScreen.main.bounds.height - 64
    var currentContainerHeight: CGFloat = 300
    
    var containerViewHeightConstraint: NSLayoutConstraint?
    private static func create(
        delegate: PopUpModalDelegate
    ) -> ModalViewController {
        let view = ModalViewController(delegate: delegate)
        return view
    }
    
    @discardableResult
    static public func present(
        initialView: UIViewController,
        delegate: PopUpModalDelegate
    ) -> ModalViewController {
        let view = ModalViewController.create(delegate: delegate)
        view.modalPresentationStyle = .overFullScreen
        view.modalTransitionStyle = .coverVertical
        initialView.present(view, animated: true)
        return view
    }
    
    public init(delegate: PopUpModalDelegate) {
        super.init(nibName: nil, bundle: nil)
        self.delegate = delegate
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public weak var delegate: PopUpModalDelegate?
    
    private lazy var canvas: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var cancelButton: UIButton = {
        let b: UIButton = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.backgroundColor = .systemRed
        b.setTitle("Cancel", for: .normal)
        b.addTarget(self, action: #selector(self.didTapCancel(_:)), for: .touchUpInside)
        return b
    }()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        
        setupPanGesture()
        
        
    }
    
    @objc func didTapCancel(_ btn: UIButton) {
        self.delegate?.didTapCancel()
    }
    
    func setupPanGesture() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGesture(gesture:)))
        panGesture.delaysTouchesBegan = false
        panGesture.delaysTouchesEnded = false
        canvas.addGestureRecognizer(panGesture)
    }
    
    func animateContainerHeight(_ height: CGFloat) {
        UIView.animate(withDuration: 0.4) {
            // Update container height
            self.containerViewHeightConstraint?.constant = height
            // Call this to trigger refresh constraint
            self.view.layoutIfNeeded()
        }
        // Save current height
        currentContainerHeight = height
    }
        
    
    @objc func handlePanGesture(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: view)
        let isDraggingDown = translation.y > 0
        let newHeight = currentContainerHeight - translation.y
        print("newHeight", newHeight)
        switch gesture.state {
        case .changed:
            // This state will occur when user is dragging
            print(newHeight)
            if newHeight < maximumContainerHeight {
                containerViewHeightConstraint?.constant = newHeight
                            // refresh layout
                            view.layoutIfNeeded()
            }
            
        case .ended:
             if newHeight < 300 {
                        // Condition 2: If new height is below default, animate back to default
                        animateContainerHeight(300)
                    }
            else if newHeight > 300 {
                        // Condition 4: If new height is below max and going up, set to max height at top
                        animateContainerHeight(maximumContainerHeight)
                    }
        default:
            break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: canvas) == true {
            return false
        }
        return true
    }
    
    private func setupViews() {
        let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapCancel))
        gesture.delegate = self
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        self.view.addGestureRecognizer(gesture)
        self.view.addSubview(canvas)
        NSLayoutConstraint.activate([
//            canvas.topAnchor.constraint(equalTo: view.topAnchor),
            canvas.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            canvas.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            canvas.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
        containerViewHeightConstraint = canvas.heightAnchor.constraint(equalToConstant: currentContainerHeight)
        containerViewHeightConstraint?.isActive = true
    }
}
