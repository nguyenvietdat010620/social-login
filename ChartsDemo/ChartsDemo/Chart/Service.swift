//
//  Service.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 13/12/2023.
//

import Foundation
import Alamofire

class Service:NSObject {
    static let shared: Service = Service()
    
    func getDetailService(completion: @escaping (Result<response, Error>) -> Void) {
        AF.request("https://xoso188.net/api/front/open/lottery/history/list/200/miba", method: .get)
            .responseDecodable(of: response.self) { response in
                switch response.result {
                case .success(let posts):
//                    print("oke post", posts)
                    completion(.success(posts))
                case .failure(let error):
                    completion(.failure(error))
//                    print("false post", error)
                }
            }
    }
    
}
