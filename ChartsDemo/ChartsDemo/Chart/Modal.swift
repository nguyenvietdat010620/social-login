//
//  Modal.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 13/12/2023.
//

import Foundation

struct ModalKQSX: Codable {
//    var turnNum: String
    var openNum: String
//    var openTime: String
//    var openTimeStamp: Int
//    var detail: String
//    var status: Int
//    var replayUrl: String?
//    var n11: String?
//    var jackpot: Int
}

struct callDay: Codable{
    var turnNum: String
    var issueList: [ModalKQSX]
}

struct response: Codable {
    var success: Bool
    var t: callDay
}
