//
//  ViewController.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 13/12/2023.
//

import UIKit
//import DGCharts

class ViewController: UIViewController, ChartViewDelegate {
    
    var barChartView: BarChartView!
    var listRes: [ModalKQSX] = []
    var listKQSX: [String] = []
    var dic: [String: Int] = [:]
    var values: [BarChartDataEntry] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        barChartView = BarChartView()
        barChartView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 300)
        barChartView.center = view.center
        view.addSubview(barChartView)
        
        barChartView.scaleYEnabled = false
        barChartView.zoom(scaleX: 1, scaleY: 0, x: 0, y: 0)
        barChartView.delegate = self
        getData()
  
        // Do any additional setup after loading the view.
    }
    
    func getData() {
        Service.shared.getDetailService { [weak self] result in
            switch result{
            case .success(let data):
                self?.listRes = data.t.issueList
                self?.converStringToString()
                self?.barChartView.renderer
            case .failure(_):
                print()
            }
        }
    }
    
    
    func converStringToString() {
        for item in listRes{
            var list = item.openNum.split(separator: ",")
            var newList = list.remove(at: 0)
            var newList1 = list.remove(at: 0)
            var newList2 = list.remove(at: 0)
            var kq = list.reduce("") { partialResult, item in
                partialResult + item
            }
            self.listKQSX.append(kq)
        }
        countTheSame(list: listKQSX)
    }
    
    func countTheSame(list: [String]) {
        for item in listKQSX{
            if(dic["\(item)"] == nil){
                dic["\(item)"] = 1
            }
            else{
                dic["\(item)"] = dic["\(item)"]! + 1
            }
        }
        for item in dic {
            values.append(BarChartDataEntry(x: Double(item.key)!, y: Double(item.value)))
        }
        
        let dataSet = BarChartDataSet(entries: values, label: "Sample Data")
        let data = BarChartData(dataSet: dataSet)
        
        // Set the data to the chart view
        barChartView.data = data
    }
    
    @IBAction func action(_ sender: Any) {
        converStringToString()
    }
    
    
    
    func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat) {
        print("chartScaled",scaleX, scaleY)
    }
}

