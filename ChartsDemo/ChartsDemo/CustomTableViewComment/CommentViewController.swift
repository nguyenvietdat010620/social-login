//
//  CommentViewController.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 15/12/2023.
//

import UIKit

struct DataComment{
    var paren: String
    var childer: [String]? = nil
}
//->
struct Comment{
    var paren: String?
    var childer: String?
}
//-> [Comment]

class CommentViewController: UIViewController {
    var listDataComment: [DataComment] = [
        DataComment(paren: "comment 1, alocomment 1, alocomment 1, alo, comment 1, alocomment 1, alocomment 1, alo"),
        DataComment(paren: "comment 2, alocomment 2, alocomment 2, alo", childer: ["chidren 1 alo alo", "chidren 2 alo alo", "comment 2, alocomment 2, alocomment 2, alo"]),
        DataComment(paren: "comment 3, alocomment 3, alocomment 3, alo", childer: ["comment 2, alocomment 2, alocomment 2, alo"]),
        DataComment(paren: "comment 3, alocomment 3, alocomment 3, alo", childer: ["comment 2, alocomment 2, alocomment 2, alo"]),
        DataComment(paren: "comment 3, alocomment 3, alocomment 3, alo", childer: ["comment 2, alocomment 2, alocomment 2, alo"]),
        DataComment(paren: "comment 3, alocomment 3, alocomment 3, alo", childer: ["comment 2, alocomment 2, alocomment 2, alo"]),
        DataComment(paren: "comment 3, alocomment 3, alocomment 3, alo", childer: ["comment 2, alocomment 2, alocomment 2, alo"]),
        DataComment(paren: "comment 3, alocomment 3, alocomment 3, alo", childer: ["comment 2, alocomment 2, alocomment 2, alo"]),
        
    ]
    var listComment:[Comment] = []
    @IBOutlet weak var commentCollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "CommentCollectionViewCell", bundle: nil)
        commentCollection.register(nib, forCellWithReuseIdentifier: "CommentCollectionViewCell")
        
        commentCollection.delegate = self
        commentCollection.dataSource = self
        
        processData()
        // Do any additional setup after loading the view.
    }
    
    func processData(){
        for item in listDataComment{
            if item.paren != nil {
                listComment.append(Comment(paren: item.paren))
            }
            if item.childer?.count != 0{
                if let listChid = item.childer {
                    for chid in listChid {
                        listComment.append(Comment(childer: chid))
                    }
                }
            }
        }
        commentCollection.reloadData()
        print(listComment.count)
    }
    

}

extension CommentViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listComment.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommentCollectionViewCell", for: indexPath as IndexPath) as! CommentCollectionViewCell
        let text = listComment[indexPath.row].paren != nil ? listComment[indexPath.row].paren : listComment[indexPath.row].childer
        let const = listComment[indexPath.row].paren != nil ? 10 : 50
        cell.label.text = text
        cell.left.constant = CGFloat(const)
        return cell
    }
    
}

extension CommentViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        var hight = listDataComment[indexPath.row].paren.count * 10
        
        return CGSize(width: UIScreen.main.bounds.width,height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
