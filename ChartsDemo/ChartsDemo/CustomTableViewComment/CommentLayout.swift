//
//  CommentLayout.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 15/12/2023.
//

import Foundation
import UIKit
class CommentLayout: UICollectionViewLayout{
    
    var contentSize:CGSize!
    let attributeArray = NSMutableDictionary()
    var numberItem: Int{
        return (collectionView?.numberOfItems(inSection: 0))!
    }
    
    var itemHeight: CGFloat{
        return (collectionView?.frame.height)!
    }
    var itemWidth:CGFloat{
        return (collectionView?.frame.width)!
    }
    
    
    override func prepare() {
        for i in 0 ... numberItem - 1 {
            var tempX : CGFloat = 10.0
            var tempY : CGFloat = 20.0
            let indexPath = NSIndexPath(item: i, section: 0)
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith:indexPath as IndexPath);
            attributes.frame = CGRect(x: tempX, y: tempY, width: itemWidth, height: itemHeight);
            self.attributeArray.setObject(attributes, forKey: indexPath)
        }
        let contentHeight = collectionView?.frame.height
        let contentWidth = collectionView?.frame.width
        self.contentSize = CGSize(width: contentWidth!, height: contentHeight!);
    }
    
    override var collectionViewContentSize: CGSize
    {
        return self.contentSize
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        
        // Duyệt các đối tượng trong attributeArray để tìm ra các cell nằm trong khung nhìn rect
        for attributes  in self.attributeArray {
            if (attributes.value as! UICollectionViewLayoutAttributes).frame.intersects(rect ) {
                layoutAttributes.append((attributes.value as! UICollectionViewLayoutAttributes))
            }
        }
        return layoutAttributes
    }
}
