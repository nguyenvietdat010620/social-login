//
//  AlamorefireConfig.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 14/12/2023.
//

import Foundation
import Alamofire

enum APIError: Error {
    case accessTokenExpired
    case networkError
    // Add more error cases as needed
}

class AlamorefireConfig {
    
}


extension AlamorefireConfig: RequestInterceptor {
    func adapt(
      _ urlRequest: URLRequest,
      for session: Session,
     completion: @escaping (Result<URLRequest, Error>) -> Void
    ) {
        
     // Called before request to set request headers
    }
    
    
    func retry(
      _ request: Request,
      for session: Session,
      dueTo error: Error,
      completion: @escaping (RetryResult) -> Void) {

         
      // Called when status code is not 200...299 or in failure
    }

}
