//
//  FireBaseViewController.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 16/12/2023.
//

import UIKit
import FirebaseCore
import FirebaseFirestore
class FireBaseViewController: UIViewController {
    var ref: DocumentReference?
    let db = Firestore.firestore()

    override func viewDidLoad() {
        super.viewDidLoad()

        read()
        // Do any additional setup after loading the view.
    }
    
    func doanyThing() {
        ref = db.collection("dat").addDocument(data: [
            "first": "Ada",
            "last": "Lovelace",
            "born": 1815
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(self.ref!.documentID)")
            }
        }
    }
    
    func read(){
        print("read")
        db.collection("dat").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("adasdas")
                    print("\(document.documentID) => \(document.data())")
                }
            }
        }
    }

}
