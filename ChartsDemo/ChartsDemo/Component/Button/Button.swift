//
//  Button.swift
//  ChartsDemo
//
//  Created by Nguyen Viet Dat on 21/12/2023.
//

import Foundation
import UIKit

class ButtonApp: UIView {
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func setupView() {
        var view = UIView()
        view.backgroundColor = UIColor.init(red: 225/255, green: 225/255, blue: 235/255, alpha: 1)
        view.layer.cornerRadius = 15
        view.frame = CGRect(x: 0, y: 0, width: 40, height: 25)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    
    
    

}
